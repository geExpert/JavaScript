var btnNewGame = document.querySelector('#newGameButton');
var btnModalClose = document.querySelector('.modal .close');
var btnModalStart = document.querySelector('#startButton');
var modal = document.querySelector('.modal');
var scoreboardDisplay = document.querySelector('.scoreboard');
var board = document.querySelector('.cells');
var messageDisplay = document.querySelector('.message');

function hideElement(element) {
    element.classList.add('hidden');
}

function showElement(element) {
    element.classList.remove('hidden');
}

function selectOpposites(player, sign) {
    var signs = document.querySelectorAll('.modal-body .signs');
    var signClass = '.' + sign.toLowerCase();
    var oppositeSignClass = signClass === '.x' ? '.o' : '.x';
    signs[0].querySelector('.highlight').classList.remove('highlight');
    signs[1].querySelector('.highlight').classList.remove('highlight');
    if (player === 0) {
        signs[0].querySelector(signClass).classList.add('highlight');
        signs[1].querySelector(oppositeSignClass).classList.add('highlight');
    } else {
        signs[1].querySelector(signClass).classList.add('highlight');
        signs[0].querySelector(oppositeSignClass).classList.add('highlight');
    }
}

function getPlayer(idx) {
    var id = 'playerName' + idx + 1;
    var name = document.querySelector('.modal form').elements[id].value;
    var signs = document.querySelectorAll('.modal-body .signs');
    var sign = signs[idx].querySelector('.highlight').classList.contains('x') ? 'X' : 'O';
    return {name: name, sign: sign};
}

function showMessage(message) {
    messageDisplay.textContent = message;
    showElement(messageDisplay);
}

function selectCurrentPlayer() {
    scoreboardDisplay.querySelector('.player.highlight').classList.remove('highlight');
    scoreboardDisplay.querySelector('.player.' + game.currentPlayer.sign.toLowerCase()).classList.add('highlight');
}

function updateScoreBoard() {
    game.players.forEach(function (player) {
        var playerNameDisplay = scoreboardDisplay.querySelector('.player.' + player.sign.toLowerCase() + ' .name');
        var scoreDisplay = scoreboardDisplay.querySelector('.score .' + player.sign.toLowerCase());

        playerNameDisplay.textContent = player.name;
        scoreDisplay.textContent = player.score;
    });
    selectCurrentPlayer();
    showElement(scoreboardDisplay);
}

function setSign(y, x, sign) {
    var cell = board.querySelector('[data-y="' + y + '"][data-x="' + x + '"]');
    cell.textContent = sign;
    cell.classList.add(sign.toLowerCase());
}


function clearBoard() {
    board.querySelectorAll('.cell').forEach(function (cell) {
        cell.textContent = '';
        cell.className = 'cell';
        var data = cell.dataset;

        var onCellClickHandler = function () {
            try {
                var msg = game.placeSign(data.y, data.x, setSign);
                if (msg.gameEnded === true) {
                    updateScoreBoard();
                }
                showMessage(msg.message);
                selectCurrentPlayer();
            }
            catch (e) {
                showMessage(e.message);
            }
        };
        cell.onclick = onCellClickHandler;
    });
    showElement(board);
}

function startGame(p1, p2) {
    var message;
    if (p1 && p2) {
        message = game.start(p1, p2);
    }
    else {
        message = game.start();
    }
    updateScoreBoard();
    showMessage(message.message);
    clearBoard();
    showElement(btnNewGame);
}


btnNewGame.addEventListener('click', function () {
    if (game.players.length) {
        startGame();
    }
    else {
        hideElement(btnNewGame);
        showElement(modal);
    }
});

btnModalClose.addEventListener('click', function () {
    showElement(btnNewGame);
    hideElement(modal);
    hideElement(document.querySelector('.modal .error-message'));
});

btnModalStart.addEventListener('click', function () {
    console.log();
    var player1 = getPlayer(0);
    var player2 = getPlayer(1);
    if (player1.name !== '' && player2.name !== '') {
        hideElement(modal);
        hideElement(document.querySelector('.modal .error-message'));
        startGame(player1, player2);
    }
    else {
        showElement(document.querySelector('.modal .error-message'));
    }

});

document.querySelectorAll('.modal .signs .x').forEach(function (el, idx) {
    el.addEventListener('click', function () {
        selectOpposites(idx, 'X');
    });
});

document.querySelectorAll('.modal .signs .o').forEach(function (el, idx) {
    el.addEventListener('click', function () {
        selectOpposites(idx, 'O');
    });
});
