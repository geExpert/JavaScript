function Player(name, sign) {
    this.name = name;
    this.sign = sign;
    this.score = 0;
}

Player.prototype.increaseScore = function () {
    return this.score++;
};

Player.prototype.setName = function (name) {
    this.name = name;
};

function Message(message, gameEnded) {
    this.message = message;
    this.gameEnded = gameEnded;
}

function Board(size) {
    this.size = size;
    this.reset();
}

Board.prototype.reset = function () {
    this.area = [];
    this.numberOfPlacedSigns = 0;
    for (var i = 0; i < this.size; i++) {
        this.area[i] = [];
        for (var j = 0; j < this.size; j++) {
            this.area[i].push(null);
        }
    }
};

Board.prototype.placeSign = function (coords, sign, callback) {
    var row = coords.row;
    var column = coords.column;
    checkBounds(row, 0, this.size, 'The row');
    checkBounds(column, 0, this.size, 'The column');
    if (this.area[row][column]) {
        throw new Error('You can\'t place a sign here, because this field is already taken');
    }
    this.area[row][column] = sign;
    this.numberOfPlacedSigns++;
    return callback();
};

Board.prototype.isBoardFull = function () {
    return Math.pow(this.size, 2) === this.numberOfPlacedSigns;
};

Board.prototype.getRows = function () {
    return this.area;
};

Board.prototype.getColumns = function () {
    var cols = [];
    for (var i = 0; i < this.size; i++) {
        cols[i] = [];
        for (var j = 0; j < this.size; j++) {
            cols[i].push(this.area[j][i]);
        }
    }
    return cols;
};

Board.prototype.getDiagonals = function () {
    var diagonals = [[], []];
    for (var i = 0; i < this.size; i++) {
        diagonals[0].push(this.area[i][i]);
    }
    for (var i = 0, j = this.size; i < this.size;) {
        diagonals[1].push(this.area[i++][--j]);
    }
    return diagonals;
};

Board.prototype.toString = function () {
    var value = '';
    var border = '+';
    for (var i = 0; i < 2 * this.size + 1; i++) {
        border += '-';
    }
    border += '+';
    value += border;
    for (var i = 0; i < this.size; i++) {
        var row = this.area[i];
        value += '\n| ';
        for (var j = 0; j < row.length; j++) {
            var sign = row[j];
            value += sign || '.';
            value += ' ';
            if (j === row.length - 1) {
                value += '|';
            }
        }
    }
    value += '\n' + border;
    return value;
};

function checkBounds() {
    var num = arguments[0],
        min = arguments[1],
        max = arguments[2],
        name = arguments[3];

    if (num < min || num >= max) {
        throw new Error(name + ' should be between ' + min + ' and ' + max + '.');
    }
}

var players = [new Player('Player 1', 'X'), new Player('Player 2', 'O')];

var game = {
    players: players,
    started: false,
    ended: false,
    currentPlayer: null,
    board: new Board(3),
    start: function (player1Name, player2Name) {
        if (player1Name) {
            this.players[0].setName(player1Name);
        }
        if (player2Name) {
            this.players[1].setName(player2Name);
        }
        this.currentPlayer = this.players[0];

        this.started = true;
        this.ended = false;
        return new Message('It\'s ' + this.currentPlayer.name + '\'s turn', false);
    },
    placeSign: function (row, column, cb) {
        var coords = {
            row: row,
            column: column
        };
        if (!this.started) {
            throw new Error('You must start a game, before you place a sign');
        }
        if (this.ended) {
            throw new Error('You can\'t place a sign, after the game is ended');
        }
        var currentMark = this.currentPlayer.sign;
        var winner = this.board.placeSign(coords, this.currentPlayer.sign, this.whoWon.bind(this));
        if (typeof cb === 'function') {
            cb(row,column,currentMark);
        }
        console.log(this.board.toString());
        if (winner) {
            this.ended = true;
            return new Message(winner.name + ' Won!', true);
        }
        if (this.board.isBoardFull()) {
            this.ended = true;
            return new Message('It\'s a Draw!', true);
        }
        this.selectNextPlayer();
        return new Message('It\'s ' + this.currentPlayer.name + '\'s turn', false);
    }
    ,
    whoWon: function () {
        var players = this.players;
        var numberOfSignsToWin = 3;
        var winner = this.board.getRows()
            .concat(this.board.getColumns())
            .concat(this.board.getDiagonals())
            .map(function (array) {
                return array.filter(function (sign) {
                    return !!sign;
                }).reduce(function (acc, sign) {
                    acc[sign]++;
                    return acc;
                }, {X: 0, O: 0});
            })
            .filter(function (count) {
                return count.X === numberOfSignsToWin || count.O === numberOfSignsToWin;
            })
            .map(function (count) {
                return count.X ? players.filter(function (player) {
                    return player.sign === 'X';
                })[0] : players.filter(function (player) {
                    return player.sign === 'O';
                })[0];
            })
            .reduce(function (acc, currentValue) {
                return acc || currentValue;
            }, null);
        return winner;
    },
    selectNextPlayer: function () {
        this.currentPlayer = this.currentPlayer === this.players[0] ? this.players[1] : this.players[0];
    },
};


console.log(game.start());
console.log(game.placeSign(0,1));