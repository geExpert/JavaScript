const players = []

const game = {
    players,
    started: false,
    ended: false,
    currentPlayer: null,
    board: [],
    start(player1, player2) {
        if (player1) {
            this.players[0].setName(player1);
        }
        if (player2) {
            this.players[1].setName(player2);
        }
        this.currentPlayer = this.players[0];
        this.started = true;
        this.ended = false;

        return 'Message'
    },

    placeSign(row, column, cb) {
        const coords = {row, column};
    },

    whoWon() {
        const players = this.players;
        const numberOfSignsToWin = 3;
        return this.board.getRows()
            .concat(this.board.getColumns())
            .concat(this.board.getDiagonals())
            .map(array => {
                return array
                    .filter(sign => !!sign)
                    .reduce((acc, currentValue) => {
                        acc[currentValue]++;
                        return acc;
                    }, {X: 0, O: 0});
            })
            .filter(count => count.X === numberOfSignsToWin || count.O === numberOfSignsToWin)
            .map(count => {
                if (count.X) {
                    return players.find(player => player.sign === 'X');
                }
                else {
                    return players.find(player => player.sign === 'O');
                }
            })[0];
    }
}