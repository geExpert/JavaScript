let btnNewGame = document.querySelector('#newGameButton');
let btnModalClose = document.querySelector('.modal .close');
let btnModalStart = document.querySelector('#startButton');
let modal = document.querySelector('.modal');
let scoreboardDisplay = document.querySelector('.scoreboard');
let board = document.querySelector('.cells');
let messageDisplay = document.querySelector('.message');

function hideElement(element) {
    element.classList.add('hidden');
}

function showElement(element) {
    element.classList.remove('hidden');
}

function selectOpposites(player, sign) {
    const signs = document.querySelectorAll('.modal-body .signs');
    const signClass = '.' + sign.toLowerCase();
    const oppositeSignClass = signClass === '.x' ? '.o' : '.x';
    signs[0].querySelector('.highlight').classList.remove('highlight');
    signs[1].querySelector('.highlight').classList.remove('highlight');
    if (player === 0) {
        signs[0].querySelector(signClass).classList.add('highlight');
        signs[1].querySelector(oppositeSignClass).classList.add('highlight');
    } else {
        signs[1].querySelector(signClass).classList.add('highlight');
        signs[0].querySelector(oppositeSignClass).classList.add('highlight');
    }
}

function getPlayer(idx) {
    const id = `playerName${idx + 1}`;
    const name = document.querySelector('.modal form').elements[id].value;
    const signs = document.querySelectorAll('.modal-body .signs');
    const sign = signs[idx].querySelector('.highlight').classList.contains('x') ? 'X' : 'O';
    return {name, sign};
}

function showMessage(message) {
    messageDisplay.textContent = message;
    showElement(messageDisplay);
}

function selectCurrentPlayer() {
    scoreboardDisplay.querySelector('.player.highlight').classList.remove('highlight');
    scoreboardDisplay.querySelector(`.player.${game.currentPlayer.sign.toLowerCase()}`).classList.add('highlight');
}

function updateScoreBoard() {
    game.players.forEach((player) => {
        const playerNameDisplay = scoreboardDisplay.querySelector(`.player.${player.sign.toLowerCase()} .name`);
        const scoreDisplay = scoreboardDisplay.querySelector(`.score .${player.sign.toLowerCase()}`);

        playerNameDisplay.textContent = player.name;
        scoreDisplay.textContent = player.score;
    });
    selectCurrentPlayer();
    showElement(scoreboardDisplay);
}

function setSign(y, x, sign) {
    const cell = board.querySelector(`[data-y="${y}"][data-x="${x}"]`);
    cell.textContent = sign;
    cell.classList.add(sign.toLowerCase());
}


function clearBoard() {
    board.querySelectorAll('.cell').forEach((cell) => {
        cell.textContent = '';
        cell.className = 'cell';
        const data = cell.dataset;

        const onCellClickHandler = () => {
            try {
                const msg = game.placeSign(data.y, data.x, setSign);
                if(msg.gameEnded === true){
                    updateScoreBoard();
                }
                showMessage(msg.message);
                selectCurrentPlayer();
            }
            catch (e) {
                showMessage(e.message);
            }
        };
        cell.onclick = onCellClickHandler;
    });
    showElement(board);
}

function startGame(p1, p2) {
    let message;
    if (p1 && p2) {
        message = game.start(p1, p2);
    }
    else {
        message = game.start();
    }
    updateScoreBoard();
    showMessage(message.message);
    clearBoard();
    showElement(btnNewGame);
}


function openModal() {
    return new Promise((resolve, reject)=>{
        hideElement(btnNewGame);
        showElement(modal);

        btnModalClose.addEventListener('click', () => {
            reject();
        });

        btnModalStart.addEventListener('click', () => {
            const player1 = getPlayer(0);
            const player2 = getPlayer(1);
            if (player1.name !== '' && player2.name !== '') {
                resolve([player1, player2]);
            }
            else {
                showElement(document.querySelector('.modal .error-message'));
            }

        });
    })
}

btnNewGame.addEventListener('click', () => {
    if (game.players.length) {
        startGame();
    }
    else {
        openModal().then((players) => {
            hideElement(modal);
            hideElement(document.querySelector('.modal .error-message'));
            startGame(players[0], players[1]);
        }, () => {
            showElement(btnNewGame);
            hideElement(modal);
            hideElement(document.querySelector('.modal .error-message'));
        });
    }
});


document.querySelectorAll('.modal .signs .x').forEach((el, idx) => {
    el.addEventListener('click', () => {
        selectOpposites(idx, 'X');
    });
});

document.querySelectorAll('.modal .signs .o').forEach((el, idx) => {
    el.addEventListener('click', () => {
        selectOpposites(idx, 'O');
    });
});
